## ocelot-online-website

Front page for [ocelot-dekstop](https://gitlab.com/cc-ru/ocelot/ocelot-desktop) emulator.

## installation
* download and install latest Rust compiler toolchain for your architecture
* ``git clone https://gitlab.com/cc-ru/ocelot/ocelot-desktop-website.git``
* ``cd ocelot-desktop-website``
* ``cargo build --release``

build results will be contained in ``./taget/release`` directory.

## configuration
configuration file will automatically create after first launch.
