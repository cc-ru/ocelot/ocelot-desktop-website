use reqwest::header::USER_AGENT;

use crate::models::gitlab_job::Root;

const OCELOT_PROJ_ID: usize = 9941848;

pub async fn get_gitlab(token: &str) -> anyhow::Result<Root> {
    let client = reqwest::Client::new();
    let body = client
        .get(format!(
            "https://gitlab.com/api/v4/projects/{}/jobs/",
            OCELOT_PROJ_ID
        ))
        .header(USER_AGENT, "ocelot-rs")
        .header("PRIVATE-TOKEN", token)
        .send()
        .await?
        .text()
        .await?;

    let gitlab_tasks: Root = serde_json::from_str(&body)?;

    Ok(gitlab_tasks)
}
