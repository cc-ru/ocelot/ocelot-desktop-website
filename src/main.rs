pub mod gitlab;
pub mod models;

use std::{convert::Infallible, sync::Arc};

use models::config::Config;
use warp::Filter;

use handlebars::Handlebars;
use serde::Serialize;
use serde_json::json;

struct WithTemplate<T: Serialize> {
    name: &'static str,
    value: T,
}

fn render<T>(template: WithTemplate<T>, hbs: Arc<Handlebars<'_>>) -> impl warp::Reply
where
    T: Serialize,
{
    let render = hbs
        .render(template.name, &template.value)
        .unwrap_or_else(|err| err.to_string());
    warp::reply::html(render)
}

fn with_template_engine(
    hb: Arc<Handlebars<'_>>,
) -> impl Filter<Extract = (Arc<Handlebars<'_>>,), Error = Infallible> + Clone {
    warp::any().map(move || hb.clone())
}

fn with_config(config: Arc<Config>) -> impl Filter<Extract = (Arc<Config>, ), Error = Infallible> + Clone {
    warp::any().map(move || config.clone())
}

pub async fn index(
    hb: Arc<Handlebars<'_>>,
    config: Arc<Config>
) -> std::result::Result<impl warp::Reply, warp::Rejection> {
    let gitlab = gitlab::get_gitlab(&config.token).await;

    let mut template = WithTemplate {
        name: "index.html",
        value: json!({ "user": "Warp" }),
    };

    if let Ok(git_info) = gitlab {
        //dbg!(git_info.clone());
        let last_update = git_info.first().unwrap().commit.clone();

        template.value = json!({
            "last_update": last_update.authored_date[..10],
            "updated_by": last_update.author_name,
            "commit_url": last_update.web_url,
            "commit": last_update.short_id
        });
    }

    Ok(render(template, hb.clone()))
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut hb = Handlebars::new();
    hb.register_template_file("index.html", "template/index.handlebars")?;

    let mut config = Config::new();
    config.load()?;
    let arc_config = Arc::new(config);



    let hb = Arc::new(hb);

    let route = warp::get()
        .and(warp::path::end())
        .and(with_template_engine(hb.clone()))
        .and(with_config(arc_config.clone()))
        .and_then(index);

    let main_filter = warp::fs::dir("static").or(route);

    warp::serve(main_filter).run((arc_config.ip, arc_config.port)).await;
    Ok(())
}
