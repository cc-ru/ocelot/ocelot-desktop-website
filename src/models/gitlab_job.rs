use serde::{Serialize, Deserialize};
use serde_json::Value;

pub type Root = Vec<Root2>;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Root2 {
    pub id: i64,
    pub status: String,
    pub stage: String,
    pub name: String,
    #[serde(rename = "ref")]
    pub ref_field: String,
    pub tag: bool,
    pub coverage: Value,
    pub allow_failure: bool,
    pub created_at: String,
    pub started_at: String,
    pub finished_at: String,
    pub duration: f64,
    pub queued_duration: f64,
    pub user: User,
    pub commit: Commit,
    pub pipeline: Pipeline,
    pub web_url: String,
    pub artifacts_file: ArtifactsFile,
    pub artifacts: Vec<Artifact>,
    pub runner: Runner,
    pub artifacts_expire_at: String,
    pub tag_list: Vec<Value>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct User {
    pub id: i64,
    pub username: String,
    pub name: String,
    pub state: String,
    pub avatar_url: String,
    pub web_url: String,
    pub created_at: String,
    pub bio: String,
    pub location: String,
    pub public_email: String,
    pub skype: String,
    pub linkedin: String,
    pub twitter: String,
    pub website_url: String,
    pub organization: String,
    pub job_title: String,
    pub pronouns: Option<String>,
    pub bot: bool,
    pub work_information: Option<String>,
    pub followers: i64,
    pub following: i64,
    pub local_time: Value,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Commit {
    pub id: String,
    pub short_id: String,
    pub created_at: String,
    pub parent_ids: Vec<String>,
    pub title: String,
    pub message: String,
    pub author_name: String,
    pub author_email: String,
    pub authored_date: String,
    pub committer_name: String,
    pub committer_email: String,
    pub committed_date: String,
    pub trailers: Trailers,
    pub web_url: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Trailers {
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Pipeline {
    pub id: i64,
    pub iid: i64,
    pub project_id: i64,
    pub sha: String,
    #[serde(rename = "ref")]
    pub ref_field: String,
    pub status: String,
    pub source: String,
    pub created_at: String,
    pub updated_at: String,
    pub web_url: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ArtifactsFile {
    pub filename: String,
    pub size: i64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Artifact {
    pub file_type: String,
    pub size: i64,
    pub filename: String,
    pub file_format: Option<String>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Runner {
    pub id: i64,
    pub description: String,
    pub ip_address: String,
    pub active: bool,
    pub paused: bool,
    pub is_shared: bool,
    pub runner_type: String,
    pub name: String,
    pub online: bool,
    pub status: String,
}
