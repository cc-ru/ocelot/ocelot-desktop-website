use std::io::{self, BufRead};
use std::path::Path;
use std::{fs::File, io::Write, net::IpAddr, str::FromStr};

#[derive(Clone)]
pub struct Config {
    pub ip: IpAddr,
    pub port: u16,
    pub token: String,
}

impl Config {
    pub fn new() -> Self {
        Self {
            ip: IpAddr::from_str("127.0.0.1").unwrap(),
            port: 8000,
            token: String::new()
        }
    }

    pub fn save(&self) -> std::io::Result<()> {
        let mut file = File::create("settings.cfg")?;
        file.write(include_str!("settings_default.cfg").as_bytes())?;
        Ok(())
    }

    pub fn load(&mut self) -> std::io::Result<()> {
        if let Ok(lines) = read_lines("settings.cfg") {
            for line in lines {
                if let Ok(ip) = line {
                    if !ip.starts_with("#") && ip.len() > 1 {
                        let setting_value = ip.splitn(2, "=").collect::<Vec<&str>>();
                        match setting_value[0] {
                        "ip" => { self.ip = IpAddr::from_str(setting_value[1]).expect("Incorrect Ip address format in config. Must be valid ipV4 or ipV6 address") }
                        "port" => { self.port = setting_value[1].parse::<u16>().expect("Incorrect port format in config. Must be integer in valid range (0-65535)") }
                        "token" => { self.token = setting_value[1].to_string(); }
                        _ => println!("warning: unknown option `{}` it will be ignored", setting_value[0])
                    }
                    }
                }
            }
        } else {
            self.save()?;
        }
        println!("configuration loaded!");
        Ok(())
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
